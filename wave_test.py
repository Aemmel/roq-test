import numpy as np
import matplotlib.pyplot as plt
import mlgw.GW_generator as generator

#generating the wave
gen = generator.GW_generator() #creating an istance of the generator (using default model)
theta = np.array((10,20,0,0)) #list of parameters to be given to generator [m1,m2,s1,s2]
times = np.linspace(-4, 0.02, 10000) #time grid: peak of 22 mode at t=0
modes = [ (2,2), (3,3), (4,4), (5,5)]
h_p, h_c = gen.get_WF(theta, times, modes) #returns amplitude and phase of the wave

theta2 = np.array((11,20,0,0)) #list of parameters to be given to generator [m1,m2,s1,s2]
h_p2, h_c2 = gen.get_WF(theta, times, modes) #returns amplitude and phase of the wave

w = np.array([times[1]-times[0] for i in range(len(times))])

h = h_p + h_c*1j
h2 = h_p2 + h_c2*1j

sp = np.fft.fft(h)
fr = np.fft.fftfreq(len(times))

#plotting the wave
plt.figure(figsize=(15,8))
plt.title("GW by a BBH with [m1,m2,s1,s2] = " + str(theta), fontsize = 15)
plt.plot(times, h_p, c='k') #plot the plus polarization
plt.xlabel("Time (s)", fontsize = 12)
plt.ylabel(r"$h_+$", fontsize = 12)
plt.show()

plt.plot(fr, sp.real, fr, sp.imag)
plt.show()