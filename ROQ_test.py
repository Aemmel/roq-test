import ROQ

import numpy as np
import mlgw.GW_generator as generator
import matplotlib.pyplot as plt
import time
import scipy.constants as consts
import pandas as pd

# data = pd.read_csv("LIGO-PSD-Test.txt", delimiter="\t", header=None)

# gen = generator.GW_generator() #creating an istance of the generator (using default model)
# theta = [  ]
# times = np.linspace(-4, 0.02, 10000) #time grid: peak of 22 mode at t=0
# modes = [ (2,2), (3,3), (4,4), (5,5) ]

# for m1 in np.arange(10, 14, 0.1):
#     for m2 in np.arange(14, 18, 0.1):
#         theta.append((m1, m2, 0, 0))

# h = [ ]
# for i in theta:
#     h_p, h_c = gen.get_WF(i, times, modes) #returns amplitude and phase of the wave
#     h.append(np.fft.fft(np.array(h_p + h_c*1j)))

# frequencies = np.fft.fftfreq(len(times))

# frequencies = frequencies[frequencies > 0]
# for i in range(len(h)):
#     h[i] = h[i][:len(frequencies)]

# weights = np.array([frequencies[1] - frequencies[0] for i in range(len(frequencies))])

# h_orig = list()
# for i in range(len(h)):
#     h_orig.append(h[i][:])

# print("initiated data")

# roq = ROQ.ROQ(frequencies, weights, theta, np.array(h), 1e-10)

# # normalize h
# # for i in range(len(roq.func_space)):
# #     roq.func_space[i] = roq.normalize(roq.func_space[i])

# t = time.process_time()
# roq_w = roq.produceROQ()[3]
# print("ROQ took " + str(time.process_time() - t) + " seconds")
# #print("Done with ROQ. Testing now with #greedy points: " + str(len(roq_w)))

# roq_w_str = "["

# for i in roq_w:
#     roq_w_str += str(i) + ","

# roq_w_str = roq_w_str[:-1] + "]"
# print(roq_w_str)

# h1 = h_orig[0]
# h2 = h_orig[10]

# print(theta[0])
# print(theta[10])

#h1_roq = np.array([h1[i] for i in roq_p])
#h2_roq = np.array([h2[i] for i in roq_p])

# plt.plot(times, np.imag(h1))
# plt.show()
# plt.plot(times, np.imag(h2))
# plt.show()

# slow_prod = ROQ.ROQ.innerProduct(weights, h1, h2)
# fast_prod = ROQ.ROQ.innerProduct(roq_w, h1, h2)

# print("Slow: " + str(slow_prod))
# print("Fast: " + str(fast_prod))



def waveform(freq, Mc):
    """ Mc in solar masses
    """
    return freq**(-7/6)*np.exp( 1j*( -consts.pi / 4. + 3. / 128. * ( consts.pi * consts.G / (consts.c**3) * freq * Mc * 1.98847e30 )**(-5/3) ) )

def S_LIGO(freq):
    y = freq[:] / 150 # copy
    return 9e-46 * ( (4.49*y)**(-56) + 0.16*y**(-4.52) + 0.52 + 0.32*y**2 )

# # # #data = pd.read_csv("LIGO-PSD-Test.txt", delimiter="\t", header=None)

f = np.linspace(40, 366.3383434841933, 10000)
# #f = np.array(data[0])

A = 2.611651689888372
# B = 10
B = 26.11651689888372
K = 3000
# # K = 500

# training_set = [A*(B / A)**(i / (K-1)) for i in range(K)]
weights = S_LIGO(f)
# # weights = np.array(data[1])

# functions = np.array([waveform(f, mu) for mu in training_set])

# normalized_functions = np.zeros(len(functions))

# roq = ROQ.ROQ(f, weights, training_set, functions, 1e-6)

# roq_x_p, roq_i_p, roq_w_short, roq_w_full, greedy_params = roq.produceROQ()

# data_roq_x_p = pd.DataFrame({ "ROQ x points":roq_x_p })
# data_roq_i_p = pd.DataFrame({ "ROQ x indices":roq_i_p })
# data_roq_w_short = pd.DataFrame({ "ROQ weights short":roq_w_short })
# data_roq_w_full = pd.DataFrame({ "ROQ weights full":roq_w_full })
# data_greedy_params = pd.DataFrame({ "Greedy Parameters":greedy_params })

# data_roq_x_p.to_csv("ROQ x points.csv", decimal=".", sep=",", index=False)
# data_roq_i_p.to_csv("ROQ x indices.csv", decimal=".", sep=",", index=False)
# data_roq_w_short.to_csv("ROQ weights short.csv", decimal=".", sep=",", index=False)
# data_roq_w_full.to_csv("ROQ weights full.csv", decimal=".", sep=",", index=False)
# data_greedy_params.to_csv("Greedy Parameters.csv", decimal=".", sep=",", index=False)

# h1 = functions[200]
# h2 = functions[1000]

# print(training_set[200])
# print(training_set[1000])

# slow_prod = ROQ.ROQ.innerProduct(weights, h1, h2)
# fast_prod = ROQ.ROQ.innerProduct(roq_w_full, h1, h2)

# print(slow_prod)
# print(fast_prod)
# exit()



roq_points_index = np.array(pd.read_csv("ROQ x indices.csv", sep=",")["ROQ x indices"], dtype=int)
roq_weights_short = np.array(pd.read_csv("ROQ weights short.csv", sep=",")["ROQ weights short"], dtype=complex)
roq_weights_full = np.array(pd.read_csv("ROQ weights full.csv", sep=",")["ROQ weights full"], dtype=complex)

# random data in training set
# one then takes the product <h_{rand_params_1[i]}, h_{rand_params_2[i]}> for all i
rand_params_1 = np.random.rand(10000)*(B-A) + A # in [A, B)
rand_params_2 = np.random.rand(len(rand_params_1))*(B-A) + A # in [A, B)

error_real = np.zeros(len(rand_params_1))
error_imag = np.zeros(len(rand_params_1))

h1 = np.array([waveform(f, i) for i in rand_params_1], dtype=complex)
h2 = np.array([waveform(f, i) for i in rand_params_2], dtype=complex)

# for cnt in range(100):
#     t = time.process_time()
#     for i in range(len(rand_params_1)):
#         # ROQ.ROQ.innerProduct(weights, h1[i], h2[i]) # accurate result
#         ROQ.ROQ.innerProduct(roq_weights_full, h1[i], h2[i]) # slow result
#     t_end = time.process_time()
#     print(t_end-t)

# ###############################################
# # generate the where
# roq_where = np.zeros(len(roq_weights_full), dtype=bool) # initialized with False
# # set True where roq_weights_full is unequal to zero
# for i in range(len(roq_weights_full)):
#     if roq_weights_full[i] != 0:
#         roq_where[i] = True

# for i in range(100):
#     t = time.process_time()
#     for i in range(len(rand_params_1)):
#         # ROQ.ROQ.innerProduct(weights, h1[i], h2[i]) # accurate result
#         ROQ.ROQ.innerProduct(roq_weights_full, h1[i], h2[i], where=roq_where) # fast result
#     t_end = time.process_time()
#     print(t_end-t)

###############################################
# with subarrays
h1_sub = np.zeros((len(h1), len(roq_points_index)), dtype=complex)
h2_sub = np.zeros((len(h2), len(roq_points_index)), dtype=complex)
for i in range(len(h1_sub)):
    for j in range(len(roq_points_index)):
        h1_sub[i][j] = h1[i][roq_points_index[j]]
        h2_sub[i][j] = h2[i][roq_points_index[j]]

# for i in range(100):
#     t = time.process_time()
#     for i in range(len(rand_params_1)):
#         ROQ.ROQ.innerProduct(weights, h1[i], h2[i]) # accurate result
#         ROQ.ROQ.innerProduct(roq_weights_short, h1_sub[i], h2_sub[i]) # fast result
#     t_end = time.process_time()
#     print(t_end-t)

for i in range(len(rand_params_1)):
    prod_fast = ROQ.ROQ.innerProduct(weights, h1[i], h2[i]) # accurate result
    prod_slow = ROQ.ROQ.innerProduct(roq_weights_short, h1_sub[i], h2_sub[i]) # fast result
    error_real[i] = np.abs(np.real(prod_fast) - np.real(prod_slow)) / np.real(prod_slow)
    error_imag[i] = np.abs(np.imag(prod_fast) - np.imag(prod_slow)) / np.imag(prod_slow)

# MIGHT PRODUCE RESULTS WITH LARGE DEVIATIONS BECAUSE OF NUMERICAL INACCURACY

print("Max Error for Real: ")
index_max_real = np.argmin(error_real)

prod_fast = ROQ.ROQ.innerProduct(weights, h1[index_max_real], h2[index_max_real]) # accurate result
prod_slow = ROQ.ROQ.innerProduct(roq_weights_short, h1_sub[index_max_real], h2_sub[index_max_real]) # fast result

print("produt between: " + str(rand_params_1[index_max_real]) + " and " + str(rand_params_2[index_max_real]))

print(prod_fast)
print(prod_slow)

print("real: " + str(np.abs(np.real(prod_fast) - np.real(prod_slow)) / np.real(prod_slow)))
print("imag: " + str(np.abs(np.imag(prod_fast) - np.imag(prod_slow)) / np.imag(prod_slow)))

# more_accurate_wave_1 = waveform(np.linspace(40, 366.3383434841933, 20000), rand_params_1[index_max_real])
# more_accurate_wave_2 = waveform(np.linspace(40, 366.3383434841933, 20000), rand_params_2[index_max_real])
# weights_more_accurate =  S_LIGO(np.linspace(40, 366.3383434841933, 20000))

# print("More accurate: ")
# print(ROQ.ROQ.innerProduct(weights_more_accurate, more_accurate_wave_1, more_accurate_wave_2))

# more_accurate_wave_1 = waveform(np.linspace(40, 366.3383434841933, 30000), rand_params_1[index_max_real])
# more_accurate_wave_2 = waveform(np.linspace(40, 366.3383434841933, 30000), rand_params_2[index_max_real])
# weights_more_accurate =  S_LIGO(np.linspace(40, 366.3383434841933, 30000))

# print("Even More accurate: ")
# print(ROQ.ROQ.innerProduct(weights_more_accurate, more_accurate_wave_1, more_accurate_wave_2))

print("--------------------------------------")
print("Max Error for Imag: ")
index_max_imag = np.argmax(error_imag)

prod_fast = ROQ.ROQ.innerProduct(weights, h1[index_max_imag], h2[index_max_imag]) # accurate result
prod_slow = ROQ.ROQ.innerProduct(roq_weights_short, h1_sub[index_max_imag], h2_sub[index_max_imag]) # fast result

print("produt between: " + str(rand_params_1[index_max_imag]) + " and " + str(rand_params_2[index_max_imag]))

print(prod_fast)
print(prod_slow)

print("real: " + str(np.abs(np.real(prod_fast) - np.real(prod_slow)) / np.real(prod_slow)))
print("imag: " + str(np.abs(np.imag(prod_fast) - np.imag(prod_slow)) / np.imag(prod_slow)))

plt.plot(error_real, label="real")
plt.plot(error_imag, label="imag")
plt.legend(loc="best")
plt.show()



# training_set = [i*0.1 for i in range(1, 100)]
# x_vals = np.linspace(0, 5, 500)
# h = np.array([1e-6*np.sin(x_vals*i) for i in training_set])
# weights = np.array([x_vals[1] - x_vals[0] for i in x_vals])

# roq = ROQ.ROQ(x_vals, weights, training_set, h, 1e-15)

# # print(roq.innerProduct(weights, h[10], h[10]))


# # exit()

# roq_w = roq.produceROQ()[3]

# h1 = h[10]
# h2 = h[40]

# print(training_set[10])
# print(training_set[40])

# roq_w_str = "["

# for i in roq_w:
#     roq_w_str += str(i) + ","

# roq_w_str = roq_w_str[:-1] + "]"
# print(roq_w_str)

# slow_prod = ROQ.ROQ.innerProduct(weights, h1, h2)
# fast_prod = ROQ.ROQ.innerProduct(roq_w, h1, h2)

# print(slow_prod)
# print(fast_prod)