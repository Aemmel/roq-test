import numpy as np
import time

def ap_list(l, n):
    for i in range(n):
        l.append(i)
    
def ap_npar(l, n):
    for i in range(n):
        l = np.append(l, i)

def sum_np(arr):
    return np.sum(arr)

def sum_for(arr):
    res = 0.0
    for i in range(len(arr)):
        res += arr[i]
    
    return res

size = 10000000
a = np.random.rand(size)
# a2 = a[:]

# ind = np.random.randint(0, high=len(a)-1, size=int(size/100))

# b = np.ones(len(a), dtype=bool)

# for i in ind:
#     a2[i] = 0
#     b[i] = False

# print("zero entries: ")
# t = time.process_time()

# res = np.sum(a2*a)

# t_end = time.process_time()
# print(res)
# print(t_end - t)



# print("where: ")
# t = time.process_time()

# res = np.sum(a*a, where=b)

# t_end = time.process_time()
# print(str(res))
# print(t_end - t)

t = time.process_time()
s = np.sum(a)
t_end = time.process_time()
print(t-t_end)
