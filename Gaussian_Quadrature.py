import numpy as np
import copy

# save polynomial as list of n values {a_0, a_1, ..., a_(n-1), a_n} for a_0 + a_1 * x + ... + a_(n-1) x^(n-1) + a_n x^n
class Polynomial:
    def __init__(self, poly=[]):
        self.coeffs = np.array(poly)
    
    # add two polynomials of different degrees
    def __add__(self, other):
        if len(self.coeffs) < len(other.coeffs):
            n_small = len(self.coeffs)
            n_large = len(other.coeffs)
            larger_pol = other.coeffs
        else:
            n_small = len(other.coeffs)
            n_large = len(self.coeffs)
            larger_pol = self.coeffs
        
        new_pol = Polynomial(np.zeros(n_large))
        for i in range(n_small):
            new_pol.coeffs[i] = self.coeffs[i] + other.coeffs[i]
        
        for i in range(n_small, n_large):
            new_pol.coeffs[i] = larger_pol[i]

        return new_pol
    
    # self - other
    def __sub__(self, other):
        return self + (-1)*other

    # multiply with scalar
    def __mul__(self, coeff):
        return Polynomial(self.coeffs * coeff)
    __rmul__ = __mul__

    # get item, if out of bounds return 0
    def __getitem__(self, key):
        if key >= len(self.coeffs):
            return 0
        
        return self.coeffs[key]
    
    # set item, if out of bounds fill up with zeros up to specified item
    def __setitem__(self, key, value):
        if key >= len(self.coeffs):
            self.coeffs = np.append(self.coeffs, np.zeros(key - len(self.coeffs) + 1))
            self.coeffs[key] = value
        else:
            self.coeffs[key] = value
    
    # call polynomial by plugging in x
    def __call__(self, x):
        res = 0
        for i in range(len(self.coeffs)):
            res += self.coeffs[i] * x**i
        
        return res
    
    # multiply with x, so e.g. x*(a+bx) = 0 + ax + bx^2
    def multWithX(self):
        self.coeffs = np.append(np.zeros(1), self.coeffs)
        return self
    
    # derivative of polynomial, return copy
    def derivative(self):
        deriv = Polynomial(np.zeros(len(self.coeffs) - 1)) # poly of deg n to poly of deg n-1

        for i in range(len(deriv.coeffs)):
            deriv[i] = (i+1)*self[i+1]
        
        return deriv

# Legendre Polynomials
legendre_polynomials = [Polynomial([1]), Polynomial([0,1])]

# create legendre polynomial (first kind) of degree n
# form of polynomials: list of n values {a_0, a_1, ..., a_(n-1), a_n} for a_0 + a_1 * x + ... + a_(n-1) x^(n-1) + a_n x^n
def legendrePolRec(n):
    global legendre_polynomials
    # if it already exists, don't compute it again
    if n < len(legendre_polynomials):
        return legendre_polynomials[n]
    
    # else, compute new one and push it into list
    # all other before will have been created, so we only need to append
    leg_pol_n_minus_1 = copy.copy(legendrePolRec(n-1))
    new_legendre_pol =  1/n * ( (2*n-1)*leg_pol_n_minus_1.multWithX() - (n - 1)*legendrePolRec(n-2) )
    legendre_polynomials.append(new_legendre_pol)

    return new_legendre_pol

def legendrePol(n):
    # create polynomial of degree n, initialized to zeros
    pol = Polynomial(np.zeros(n+1))

    fact = np.math.factorial
    for i in range(int(np.floor(n / 2.)) + 1):
        fac = fact(2*n - 2*i) / ( fact(n-i)*fact(n-2*i)*fact(i) * 2**n)
        if i % 2 != 0:
            fac *= -1
        
        pol.coeffs[n-2*i] = fac
    
    return pol

# find zero of polynomial using newtons method
# start at x0, until epsilon is reached
# don't step out of range of max_val (around x0), otherwise return np.inf
def newtonZero(pol, x0, epsilon, max_val):
    x = x0

    while np.abs(pol(x)) > epsilon:
        x -= pol(x) / pol.derivative()(x)

        if np.abs(x0 - x) > max_val:
            return np.inf
    
    return x

# find n zeros between (0, 1] of a polynomial using newtons method (up to epsilon_y)
# consider zeros which are up a distance of epsilon_x apart the same
def findZerosOnUnit(pol, n, epsilon_y, epsilon_x):
    step_size = 1.0 / n # starting step size, half every iteration

    cnt = 0
    zeros = []
    while len(zeros) < n and step_size > epsilon_x:
        for i in np.arange(0, 1, step_size):
            new_zero = newtonZero(pol, i, epsilon_y, 1)
            # if already found or zero, skip
            if new_zero > 0 and containsUpToEps(zeros, new_zero, epsilon_x) == False and new_zero > epsilon_x and new_zero != np.inf:
                zeros.append(new_zero)
        
        step_size /= 2.0
        cnt += 1
        # print("halfed step_size #times: " + str(cnt))
    
    return zeros

# vals contains x up to accuracy epsilon?
def containsUpToEps(vals, x, eps):
    for i in vals:
        if np.abs(i - x) < eps:
            return True
    
    # no value was found
    return False

n = 21 #21
leg_pol = legendrePol(n)
zeros = np.array(findZerosOnUnit(leg_pol, np.floor(n/2), 1e-10, 1e-8))
zeros = np.append(np.flip(-1*zeros), np.append(np.array([0]), zeros))

weights = np.zeros(n)
for i in range(n):
    weights[i] = 2 / ( (1-zeros[i]**2) * (leg_pol.derivative()(zeros[i]))**2 )

test_func = lambda x : 1 / (1 + 25*x**2)

res = 0.0
for i in range(n):
    res += weights[i] * test_func(zeros[i])

print(res)
