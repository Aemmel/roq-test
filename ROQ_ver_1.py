import numpy as np
import mlgw.GW_generator as generator
import matplotlib.pyplot as plt
import time

class ROQ:
    """
    Integral Solver using the Reduced Order Quadratures method
    ...
    """
    def __init__(self, quad_points, quad_weights, training_set, functions, epsilon):
        """
        quad_points, quad_weights: np.array
        training set: list of tuples
        functions: dict{tuple:np.array(dtype=complex)}
        epsilon: float value

        quad_points, quad_weights have same length M
        It is assumed, that functions is a dict of (np)arrays of length M, 
            representing the function evaluated at quad_points
        """
        self.quad_points = np.array(quad_points)
        self.quad_weights = np.array(quad_weights)
        self.training_set = training_set
        self.functions = functions
        self.epsilon = epsilon

        # all have to have the same length. It is assumed that every item of functions has the same length
        if not len(quad_points) == len(quad_weights) and not len(quad_points) == len(functions[training_set[0]]):
            print("length of quad_points, quad_weghts and of functions elements do not match!")
            return
        self.M = len(quad_points)

        # dtype of elements of function
        self.func_element_type = self.functions[self.training_set[0]].dtype
        print(self.func_element_type)

    def innerProduct(self, func_1, func_2):
        """
        Discrete L_2 inner product using the quadrature points and weights provided to the class
        <f1, f2> = sum_(k=1)^M quad_weights_k * complexConjugate(f1)(quad_points_k) * f2(quad_points_k)
        """
        return np.sum(self.quad_weights * np.conjugate(func_1) * func_2)

    def norm(self, func):
        """
        Norm of function using the discrete L_2 inner product with the provided quadrature points and weights
        """
        return np.sqrt(np.abs(self.innerProduct(func, func)))
    
    def normalize(self, func):
        """
        Returns the normalized function func
        """
        return func / self.norm(func)
    
    def project(self, basis, func):
        """
        Project func onto subspace spanned by basis
        basis: list of functions
        """
        proj = np.zeros(self.M, self.func_element_type)

        for i in basis:
            proj += self.innerProduct(i, func) * i
        
        return proj

    def ROQ(self):
        """
        Algorithm 2
        Construction of the reduced order quadratue points and weigts

        Path #2, so we wirst approximate F, then F_n² and then F^Tilde (= F_t) (t indicates the tilde)

        return: ROQ_points (np.array), ROQ_weights (np.array)
        """
        # approximate F
        reduced_basis, greedy_params = self.RBGreedy()
        print("done with RBGreedy, Basis of length " + str(len(reduced_basis)) + " remaining")

        # print("How good is the approximation: ")
        # test_f = self.functions[self.training_set[8]]
        # print("length of old basis " + str(len(self.training_set)))
        # print("length of new basis " + str(len(greedy_params)))
        # print(test_f)
        # print(self.project(reduced_basis, test_f))
        # print(self.norm(test_f - self.project(reduced_basis, test_f)))
        # VERDICT: APPROXIMATION OF F IS WORKING

        # approximate F_t (F_n² is approximated inside of twoStepRBGreedy)
        reduced_basis_t, greedy_params_t = self.twoStepRBGreedy(reduced_basis, greedy_params) # We don't really need the greedy parameters. Might be worth not calculating them at all
        print("done with twoStepRBGreedy, basis of length " + str(len(reduced_basis_t)) + " remaining")

        # print("How good is the approximation: ")
        # test_f = self.functions[self.training_set[0]]*self.functions[self.training_set[11]]
        # print("length of old basis " + str(len(greedy_params)**2))
        # print("length of new basis " + str(len(greedy_params_t)))
        # #print(test_f)
        # #print(self.project(reduced_basis, test_f))
        # print(self.norm(test_f - self.project(reduced_basis_t, test_f)))
        # VERDICT: APPROXIMATION OF F_t IS WORKING

        # V = [e_1, e_2, ..., e_m] with e_i reduced basis for F_t as columns (M-point arrays)
        V = np.array([e for e in reduced_basis_t]).transpose()

        # DEIM Matrix and points (which serve as the ROQ points)
        DEIM_mat, ROQ_points = self.DEIM(V)

        print("done with DEIM")

        # ROQ weights
        # wROQ^T = w^T . V . (P^T . V)^-1 
        ROQ_weights = (self.quad_weights.transpose() @ V @ np.linalg.inv(DEIM_mat.transpose() @ V)).transpose()

        return ROQ_points, ROQ_weights

    def RBGreedy(self):
        """
        Algorithm 4

        returns: reduced_basis (for F) (list of np.arrays), greedy_parameters (for F) (list of np.arrays)
        """
        n = 0
        sigma = 1

        # lists are better here than np.arrays since lists can be appended to much faster
        # and the final size is not known beforehand
        mu = [ self.training_set[0] ] # arbitrary, training_set does not have to be sorted
        e = [ self.functions[mu[0]] ]
        e[0] = self.normalize(e[0]) # functions elements do not have to be normalized, our basis is supposed to be

        while sigma >= self.epsilon:
            # build failsafe? if epsilon is too small, then it should break off at some point
            n += 1
            sigmas = np.array([self.norm(self.functions[i] - self.project(e, self.functions[i])) for i in self.training_set]) # largest error for current subspace e
            index_max = np.argmax(sigmas)
            sigma = sigmas[index_max]
            print("Error Greedy: " + str(sigma))
            mu.append(self.training_set[index_max])
            e.append(self.functions[mu[n]] - self.project(e, self.functions[mu[n]])) # project onto old basis, mu[n] is guaranteed to be latest element
            e[n] = self.normalize(e[n]) # normalize element
        
        return e, mu

    def project_fast(self, basis, func, func_ind, prod_dict):
        """
        basis to project onto, function func to project, func_ind which is the unique index (or generally identifier) of func, 
        prod_dict is the dictionary mapping the index of the function and the basis element (e_i in basis) (i, func_ind) to the inner product <e_i, func>
        """
        proj = np.zeros(self.M, self.func_element_type)

        for i in range(len(basis)):
            #if not (i, func_ind) in prod_dict:
            #    prod_dict[(i, func_ind)] = self.innerProduct(basis[i], func)

            proj += prod_dict[(i, func_ind)] * basis[i]
        
        return proj

    def twoStepRBGreedy(self, reduced_basis, greedy_params):
        """
        Algorithm 1

        returns: reduced_basis (for F_t) (list of np.arrays), greedy_parameters (for F_t) (list)
        """
        # approximate T²_K = T_K x T_K and define F_n²
        # T2 = np.array([[(greedy_params[i], greedy_params[j]) for j in range(len(greedy_params))] for i in range(len(greedy_params))])
        # T2 = T2.flatten() # flattened array is easier to work with
        T2 = [ ]
        for i in range(len(greedy_params)):
            for j in range(len(greedy_params)):
                T2.append((greedy_params[i], greedy_params[j]))
        F2 = [ ]
        for i in range(len(reduced_basis)):
            for j in range(len(reduced_basis)):
                F2.append(self.normalize(np.conj(reduced_basis[i]) * reduced_basis[j]))

        print("Length of Fn x Fn before 2 Step Greedy: " + str(len(F2)))

        m = 0
        sigma = 1
        mu = [ T2[0] ] # arbitrary choice, T2 does not have to be sorteed
        e = [ F2[0] ] # all elements in F2 are normalized by construction

        # keep track of what projections were already calculated
        inner_products = dict()
        projected_funcs = np.zeros((len(F2), len(F2[0])), dtype=complex)

        while sigma >= self.epsilon:
            m += 1
            # project onto old basis
            for f in range(len(F2)):
                projected_funcs[f] += self.innerProduct(e[m-1], F2[f]) * e[m-1]
            sigmas = np.array([self.norm(F2[i] - projected_funcs[i]) for i in range(len(F2))])
            # projected_funcs is only used in conjunction with F2[i], so might be even better to just store F2[i] - projected_funcs[i] for every i as one big array
            # then it would be -= self.innerProduct(e[m-1], F2[f]) * e[m-1] instead and it could directly be used is sigmas and e.append.
            index_max = np.argmax(sigmas) # find maximum error
            sigma = sigmas[index_max]
            print("Error 2 Step Greedy: " + str(sigma))
            mu.append(T2[index_max])
            e.append(F2[index_max] - projected_funcs[index_max]) # project onto old basis
            e[m] = self.normalize(e[m])

        # while sigma >= self.epsilon:
        #     m += 1
        #     t = time.process_time()
        #     sigmas = np.array([self.norm(F2[i] - self.project_fast(e, F2[i], i, inner_products)) for i in range(len(F2))])
        #     #   project_fast speeds it up by quite a bit (roughly 30 to 40% at least) but the limiting factor
        #     #   is the error calculation. Even when calculating the inner products take constant time (which it does)
        #     #   the fact that the basis gets larger means the calculation still gets larger and larger... 
        #     #   maybe there is a way to speed even that up, but I am not sure how without having huge memory costs, if one 
        #     #   were to not only remember 
        #     # sigmas = np.array([self.norm(f - self.project(e, f)) for f in F2])
        #     print("Calculating sigmas took: " + str(time.process_time() - t))
        #     t = time.process_time()
        #     index_max = np.argmax(sigmas) # find maximum error
        #     sigma = sigmas[index_max]
        #     print("Error 2 Step Greedy: " + str(sigma))
        #     mu.append(T2[index_max])
        #     e.append(F2[index_max] - self.project_fast(e, F2[index_max], index_max, inner_products)) # project onto old basis
        #     # e.append(F2[index_max] - self.project(e, F2[index_max])) # project onto old basis
        #     print("Basis length: " + str(len(e)) + " and dict length: " +  str(len(inner_products)))
        #     e[m] = self.normalize(e[m])
        #     print("And rest took: " + str(time.process_time() - t))
        #     print("----")
        #     print()
        
        return e, mu

    def DEIM(self, V):
        """
        Algorithm 5

        V (Mxm)-matrix as defined in paper. Column vectors fo V must be linearly independent

        returns: interpolation matrix, interpolation points
        """
        # already bring them into the correct final shape to avoid costly appending
        j = np.argmax(np.abs(V[:, 0])) # j in [0, M)
        U = np.zeros(np.shape(V), dtype=self.func_element_type) # Mxm matrix
        P = np.zeros(np.shape(V)) # Mxm matrix
        p = np.zeros(np.shape(V)[1], dtype=int) # m vector

        # init U, P, p
        U[:, 0] = V[:, 0] # e_1 as first column
        P[j, 0] = 1 # unit column vector with single unit entry at index j
        p[0] = j    # store index of the DEIM points, instead of the points themselves
                    # this will make it easier to relate the points to the function values

        for i in range(1, len(p)): # from 1 to m-1
            c = np.linalg.solve(P[:, :i].transpose() @ U[:, :i], P[:, :i].transpose() @ V[:, i]) # solve (P^T . U) c = (P^T) e_1 for c
            r = V[:, i] - U[:, :i] @ c
            j = np.argmax(np.abs(r))
            U[:, i] = r
            P[j, i] = 1 # we only want unit columns, the rest is already 0
            p[i] = j
            print("Ready with i=" + str(i))
        
        return P, p

# training_set = list()
# for i in np.arange(1, 3, 0.1):
#     training_set.append(i)
# h = dict()
# x_arr = np.linspace(0, 1, 2000)
# for mu in training_set:
#     h[mu] = np.sin(mu*x_arr)
# weights = [ x_arr[1]-x_arr[0] for i in x_arr]

# print("Training set: " + str(training_set))

# roq = ROQ(x_arr, weights, training_set, h, 1e-6)

# roq_p, roq_w = roq.ROQ()
# print("old length: " + str(len(x_arr)))
# print("new length: " + str(len(roq_p)))

# # integrate two functions from training set
# mu_1 = training_set[5]
# mu_2 = training_set[10]
# print("mu1: " + str(mu_1) + ", mu2: " + str(mu_2))
# h1 = h[mu_1]
# h2 = h[mu_2]

# # only keep ROQ points, out of order but it doesn't matter, as long as it matches with the ROQ points
# h1_roq = np.array([h1[i] for i in roq_p])
# h2_roq = np.array([h2[i] for i in roq_p])

# print(np.sum(weights * np.conj(h1) * h2))
# print(np.sum(roq_w * np.conj(h1_roq) * h2_roq))

# # integrate two functions NOT from training set
# g1 = np.sin(1.05*x_arr)
# g2 = np.sin(2.85*x_arr)
# g1_roq = np.array([g1[i] for i in roq_p])
# g2_roq = np.array([g2[i] for i in roq_p])

# print(np.sum(roq_w * np.conj(g1_roq) * g2_roq))

def prod(f1, f2, weights):
    return np.sum(weights * np.conj(f1) * f2)

gen = generator.GW_generator() #creating an istance of the generator (using default model)
theta = [  ]
times = np.linspace(-4, 0.02, 15000) #time grid: peak of 22 mode at t=0
modes = [ (2,2), (3,3), (4,4), (5,5) ]

for m1 in np.arange(10, 14, 0.5):
    for m2 in np.arange(14, 18, 0.5):
        theta.append((m1, m2, 0, 0))

h = dict()
for i in theta:
    h_p, h_c = gen.get_WF(i, times, modes) #returns amplitude and phase of the wave
    h[i] = np.fft.fft(np.array(h_p + h_c*1j))

frequencies = np.fft.fftfreq(len(times))

frequencies = frequencies[frequencies > 0]
for i in h:
    h[i] = h[i][:len(frequencies)]

weights = np.array([frequencies[1] - frequencies[0] for i in range(len(frequencies))])

h_orig = dict()
for i in h:
    h_orig[i] = h[i]

print("initiated data")

roq = ROQ(frequencies, weights, theta, h, 1e-5)

# normalize h
for i in roq.functions:
    roq.functions[i] = roq.normalize(roq.functions[i])

t = time.process_time()
roq_p, roq_w = roq.ROQ()
print("ROQ took " + str(time.process_time() - t) + " seconds")
print("Done with ROQ. Testing now with #greedy points: " + str(len(roq_p)))

h1 = h_orig[theta[0]]
h2 = h_orig[theta[10]]

print(theta[0])
print(theta[10])

h1_roq = np.array([h1[i] for i in roq_p])
h2_roq = np.array([h2[i] for i in roq_p])

# plt.plot(times, np.imag(h1))
# plt.show()
# plt.plot(times, np.imag(h2))
# plt.show()

print("Slow: " + str(prod(h1, h2, weights)))
print("Fast: " + str(prod(h1_roq, h2_roq, roq_w)))